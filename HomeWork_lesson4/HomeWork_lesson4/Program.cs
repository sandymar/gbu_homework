﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_lesson4
{
    class Program
    {
        /// <summary>
        /// Образец обобщенного списка
        /// </summary>
        private static List<Order> ordersList;
        /// <summary>
        /// Образец словаря
        /// </summary>
        private static Dictionary<string, int> dict;
        
        /// <summary>
        /// Точка входа в программу
        /// </summary>
        static void Main(string[] args)
        {
            dict = new Dictionary<string, int>(){
                {"four",4 },
                {"two",2 },
                { "one",1 },
                {"three",3 },
            };

            ordersList = new List<Order>()
            {
                new Order(new DateTime(2018,1,1),1,"James"),
                new Order(new DateTime(2018,1,3),2,"Hank"),
                new Order(new DateTime(2018,1,5),3,"Hank"),
                new Order(new DateTime(2018,1,5),4,"James"),
                new Order(new DateTime(2018,1,6),5,"James"),
                new Order(new DateTime(2018,1,7),6,"Hank"),
                new Order(new DateTime(2018,1,8),7,"James"),
                new Order(new DateTime(2018,1,14),9,"James"),
                new Order(new DateTime(2018,1,19),10,"Hank"),
                new Order(new DateTime(2018,1,21),11,"Hank"),
                new Order(new DateTime(2018,1,21),12,"James"),
            };

            IntListCounter();
            GenericListCounter();
            LinqListCounter();
            LambdaDictionaryOrder();
            DelegateDictionaryOrder();

            Console.ReadLine();
        }

        /// <summary>
        /// Подсчет количества дублей элементов в необобщенной коллекции
        /// </summary>
        private static void IntListCounter()
        {
            Console.WriteLine("------------------------");
            "Необобщенная коллекция".WriteBeauty();
            ArrayList intList = new ArrayList() { 15, 3, 4, 6, 8, 4, 2, 7, 4, 3, 2 };
            intList.Sort();
            int _counter = 0;
            int _actValue = 0;

            foreach (int i in intList)
            {
                if (_actValue != i)
                {
                    if (_counter != 0) $"Число {_actValue} встречается {_counter} раз".WriteBeauty();
                    _actValue = i;
                    _counter = 0;
                }
                _counter++;
            };
            if (_actValue != 0) $"Число {_actValue} встречается {_counter} раз".WriteBeauty();
        }

        /// <summary>
        /// Подсчет количества дублей элементов в обобщенной коллекции без LINQ
        /// </summary>
        private static void GenericListCounter()
        {
            Console.WriteLine("------------------------");
            "Для обобщенной коллекции без LINQ".WriteBeauty();
            IEnumerable<Order> sortedOrders = ordersList.OrderBy(e => e.EmployeeName);
            Order _actValue = null;
            int _counter = 0;

            foreach (Order employ in sortedOrders)
            {
                if (_actValue?.EmployeeName != employ.EmployeeName)
                {
                    if (_counter != 0) $"Сотрудник по имени {_actValue.EmployeeName} оформил {_counter} заказов ".WriteBeauty();
                    _actValue = employ;
                    _counter = 0;
                }
                _counter++;
            };
            if (_actValue != null) $"Сотрудник по имени {_actValue.EmployeeName} оформил {_counter} заказов".WriteBeauty();
        }

        /// <summary>
        /// Подсчет количества дублей элементов в обобщенной коллекции с использованием LINQ
        /// </summary>
        private static void LinqListCounter()
        {

            Console.WriteLine("------------------------");
            "С применением LINQ".WriteBeauty();
            var uniqueOrders = from employ in ordersList
                               orderby employ.EmployeeName
                               group employ by employ.EmployeeName into groupeEmp
                               select new { name = groupeEmp.Key, orderCount = groupeEmp.Count() };

            foreach (var employ in uniqueOrders) $"Сотрyдник по имени {employ.name} оформил {employ.orderCount} заказов".WriteBeauty();
        }

        /// <summary>
        /// Свертка механизма сортировки словаря с использованием лямбда-выражений
        /// </summary>
        private static void LambdaDictionaryOrder()
        {
            Console.WriteLine("------------------------");
            "Сортировка и вывод с применением лямбда-выражения".WriteBeauty();
            dict.OrderBy(e => e.Value).ToList().ForEach(e=> $"{e.Key} - {e.Value}".WriteBeauty());   
        }

        /// <summary>
        /// Расширение обращения в словарю с использованием предиката
        /// </summary>
        private static void DelegateDictionaryOrder()
        {
            //"Развернуть обращение к OrderBy с использованием делегата Predicate<T>"
            //Мне кажется, формулировка либо неполна, либо некорректна.
            //OrderBy не использует предикаты для определения порядка сортировки
            //Буду благодарна за комментарии по этому пункту
        }
    }
}
