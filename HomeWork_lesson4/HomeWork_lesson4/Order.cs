﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_lesson4
{
    class Order
    {
        private DateTime _date;
        private int _number;
        private string _employeeName;
        /// <summary>
        /// Дата заказа
        /// </summary>
        public DateTime Date { get;}
        /// <summary>
        /// Номер заказа
        /// </summary>
        public int Number { get;}
        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string EmployeeName { get;}

        /// <summary>
        /// Создать новый заказ
        /// </summary>
        /// <param name="date">Дата заказа</param>
        /// <param name="number">Номер заказа</param>
        /// <param name="employeeName">Имя сотрудника</param>
        public Order(DateTime date,int number, string employeeName)
        {
            Date         = date;
            Number       = number;
            EmployeeName = employeeName;
        }

        /// <summary>
        /// Выводит имя сотрудника из текущего заказа
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return EmployeeName;
        }

    }
}
