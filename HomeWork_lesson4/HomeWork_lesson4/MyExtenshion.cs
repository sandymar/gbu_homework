﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeWork_lesson4
{
    /// <summary>
    /// Расширение для работы со строками
    /// </summary>
    static class MyExtenshion
    {
        /// <summary>
        /// Расширение реализует посимвольный вывод данных в консоль с указанной задержкой
        /// </summary>
        /// <param name="text">Выводимая строка</param>
        /// <param name="sleep">Интервал вывода символов</param>
        static public void WriteBeauty(this string text,int sleep=20)
        {
            foreach (char c in text)
            {
                Console.Write(c);
                Thread.Sleep(sleep);
            };
            Console.WriteLine();
        }

        /// <summary>
        /// Расширение реализует посимвольный вывод данных в консольь с указанной задержкой
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Коллекция для вывода</param>
        /// <param name="sleep">Интервал вывода символов</param>
        static public void WriteBeauty<T>(this List<T> collection, int sleep=20)
        {
            collection.ForEach(e =>
            {
                Console.Write($"{e.ToString()} ");
                Thread.Sleep(sleep);
            });
            Console.WriteLine();
        }
    }
}
